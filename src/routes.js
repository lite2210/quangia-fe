import ProjectChecklist from './components/ProjectChecklist.vue';

const routes = [
    // { path: '/', component: Home },
    {
        path: '/project', component: ProjectChecklist, name: 'project-checklist'
    }
];

export default routes;

